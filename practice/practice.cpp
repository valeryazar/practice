﻿/**
* @file practice.cpp
* @author Азаров В.В., группа 515А.
* @date 22.06.2019
* @brief Ппрактическая работа.
*
* Задание 1.
* Проектирование и разработка программы на языке программирования С. Использование Git.
**/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <locale.h>

int main()
{
	float X, Y, X1, Y1;
	float A, B, C, A1, B1, C1;
	setlocale(LC_ALL, "rus"); 
	printf("Ввведите координаты точки: ");
	scanf("%f %f", &X, &Y);
	printf("Введите коэффициенты прямой ax + by + c = 0 через пробел: ");
	scanf("%f %f %f", &A, &B, &C);
	if (Y == 0) {
		X1 = X;
		Y1 = Y;
		goto get_the_coordinates_1;
	}
	else if (A == 0 && B == 0) {
		X1 = X;
		Y1 = -Y;
		goto get_the_coordinates_2;
	}
	else if (A == 0) {
		X1 = X;
		Y1 = -Y;
		goto get_the_coordinates_3;
	}
	else if (B == 0) {
		X1 = -(C / A);
		Y1 = Y;
	}
	else {
		A1 = B;
		B1 = -A;
		C1 = -B * X + Y * A;
		Y1 = (A1 * C / A - C1) / (B1 - A1 * B / A);
		X1 = (-B * Y1 - C) / A;
	}
	X1 = X1 * 2 - X;
	Y1 = Y1 * 2 - Y;
get_the_coordinates_1: get_the_coordinates_2: get_the_coordinates_3:
	printf("\nКоординаты зеркальной точки\n");
	printf(" x1 = %.2f\n", X1);
	printf(" y1 = %.2f\n", Y1);
	return 0;
}